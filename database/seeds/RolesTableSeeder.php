<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
            	'title' => 'Administrator',
            	'slug' => 'administrator'
	        ],
			[
            	'title' => 'Uljara',
            	'slug' => 'uljara'
	        ],
			[
            	'title' => 'Prijevoznik',
            	'slug' => 'prijevoznik'
	        ]
        ]);
    }
}
