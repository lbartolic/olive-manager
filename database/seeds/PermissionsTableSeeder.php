<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'title' => 'Pregled predbilježbi',
                'slug' => 'pregled-predbiljezbi'
            ],
            [
                'title' => 'Upravljanje predbilježbama',
                'slug' => 'upravljanje-predbiljezbama'
            ]
        ]);
    }
}
