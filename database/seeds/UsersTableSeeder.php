<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            	'name' => 'Administrator',
            	'email' => 'admin@admin.com',
            	'password' => bcrypt('password'),
            	'role_id' => 1,
	        ],
			[
            	'name' => 'Uljko',
            	'email' => 'uljara@uljara.com',
            	'password' => bcrypt('password'),
            	'role_id' => 2,
	        ],
	        [
            	'name' => 'Prijevoznik',
            	'email' => 'prijevoznik@prijevoznik.com',
            	'password' => bcrypt('password'),
            	'role_id' => 3,
	        ]
        ]);
    }
}
