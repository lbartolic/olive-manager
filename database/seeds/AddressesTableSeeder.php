<?php

use Illuminate\Database\Seeder;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('addresses')->insert([
            [
            	'street' => 'Antuna Augustinčića 22',
            	'city' => 'Čakovec',
            	'country' => 'Croatia',
            	'postal_code' => 40000,
	        ],
            [
                'street' => 'Travnik 39',
                'city' => 'Čakovec',
                'country' => 'Croatia',
                'postal_code' => 40000,
            ],
            [
                'street' => 'Travnik 15',
                'city' => 'Čakovec',
                'country' => 'Croatia',
                'postal_code' => 40000,
            ],
            [
                'street' => 'Ulica Svete Ane 15b',
                'city' => 'Osijek',
                'country' => 'Croatia',
                'postal_code' => 31000,
            ],
            [
                'street' => 'Petračićeva 4',
                'city' => 'Zagreb',
                'country' => 'Croatia',
                'postal_code' => 10000,
            ]
        ]);
    }
}
