<?php

use Illuminate\Database\Seeder;

class WorkingHoursConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('config')->insert([
            [
                'key' => 'Ponedjeljak',
                'value' => '8'
            ],
            [
                'key' => 'Utorak',
                'value' => '8'
            ],
            [
                'key' => 'Srijeda',
                'value' => '8'
            ],
            [
                'key' => 'Četvrtak',
                'value' => '8'
            ],
            [
                'key' => 'Petak',
                'value' => '8'
            ],
            [
                'key' => 'Subota',
                'value' => '8'
            ],
            [
                'key' => 'Nedjelja',
                'value' => '8'
            ],
            [
                'key' => 'Kapacitet stroja',
                'value' => '1400'
            ],
            [
                'key' => 'Kapacitet vozila',
                'value' => '1400'
            ],
            [
                'key' => 'Koeficijent pretvorbe (L -> g)',
                'value' => '916'
            ],
        ]);
    }
}
