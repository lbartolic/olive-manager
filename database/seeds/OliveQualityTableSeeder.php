<?php

use Illuminate\Database\Seeder;

class OliveQualityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('olive_quality')->insert([
            [
            	'quality' => 'Nisko'
	        ],
			[
            	'quality' => 'Srednje'
	        ],
			[
            	'quality' => 'Visoko'
	        ]
        ]);
    }
}
