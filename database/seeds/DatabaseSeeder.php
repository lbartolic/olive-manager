<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OilQualityTableSeeder::class);
        $this->call(OliveQualityTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(AddressesTableSeeder::class);
        $this->call(CooperantsTableSeeder::class);
        $this->call(WorkingHoursConfigSeeder::class);
    }
}
