<?php

use Illuminate\Database\Seeder;

class CooperantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cooperants')->insert([
            [
            	'first_name' => 'Luka',
            	'last_name' => 'Bartolić',
            	'company' => 'My Company',
            	'OIB' => '1234567890',
            	'address_id' => 1,
            	'phone' => '040/367890',
            	'mobile_phone' => '+385987654321',
            	'email' => 'email@email.com',
            	'comment' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            	'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
				'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
	        ],
            [
            	'first_name' => 'Ivan',
            	'last_name' => 'Bernatović',
            	'company' => 'Company Name',
            	'OIB' => '0987654321',
            	'address_id' => 2,
            	'phone' => '040/123345',
            	'mobile_phone' => '+385989876543',
            	'email' => 'mail@email.com',
            	'comment' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            	'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
				'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
	        ],
	        [
            	'first_name' => 'Dominik',
            	'last_name' => 'Kotris',
            	'company' => 'My Company',
            	'OIB' => '6537891234',
            	'address_id' => 3,
            	'phone' => '040/546789',
            	'mobile_phone' => '+385981234567',
            	'email' => 'mymail@email.com',
            	'comment' => 'Lorem ipsum dolor sit amet.',
            	'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
				'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
	        ],
            [
            	'first_name' => 'Test',
            	'last_name' => 'User',
            	'company' => 'My Company',
            	'OIB' => '1234567890',
            	'address_id' => 4,
            	'phone' => '040/367890',
            	'mobile_phone' => '+385987654321',
            	'email' => 'email@email.com',
            	'comment' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            	'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
				'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
	        ],
            [
            	'first_name' => 'User',
            	'last_name' => 'Test',
            	'company' => 'Company Name',
            	'OIB' => '0987654321',
            	'address_id' => 5,
            	'phone' => '040/123345',
            	'mobile_phone' => '+385989876543',
            	'email' => 'mail@email.com',
            	'comment' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            	'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
				'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
	        ]
        ]);
    }
}
