<?php

use Illuminate\Database\Seeder;

class OilQualityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oil_quality')->insert([
            [
            	'quality' => 'Djevičansko'
	        ],
			[
            	'quality' => 'Ekstra djevičansko'
	        ]
        ]);
    }
}
