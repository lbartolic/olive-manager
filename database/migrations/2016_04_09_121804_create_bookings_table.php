<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cooperant_id')->unsigned()->nullable();
            $table->integer('address_id')->unsigned()->nullable();
            $table->dateTime('processing_start');
            /**
             * Olive amount in kilograms [kg].
             */
            $table->integer('olive_amount');
            /** 
             * Transport:
             * 0 - no transport
             * 1 - transport service
             */
            $table->tinyInteger('transport');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('cooperant_id')
                ->references('id')
                ->on('cooperants')
                ->onDelete('restrict');
            $table->foreign('address_id')
                ->references('id')
                ->on('addresses')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
    }
}
