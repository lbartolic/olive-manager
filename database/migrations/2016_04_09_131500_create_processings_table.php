<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processings', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('processing_start');
            $table->dateTime('processing_end');
            /**
             * Olive amount in grams [g].
             */
            $table->integer('olive_amount');
            /**
             * Oil amount in litres [l].
             */
            $table->integer('oil_amount');
            $table->integer('olive_quality_id')->unsigned()->nullable();
            $table->integer('oil_quality_id')->unsigned()->nullable();
            $table->integer('booking_id')->unsigned();
            $table->timestamps();

            $table->foreign('olive_quality_id')
                ->references('id')
                ->on('olive_quality')
                ->onDelete('restrict');
            $table->foreign('oil_quality_id')
                ->references('id')
                ->on('oil_quality')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('processings');
    }
}
