@extends("layouts.master")

@section("title", "Kooperanti")

@section("main-content")
		<table class="table-custom">
			<thead>
				<tr>
					<th>Ime i prezime</th>
					<th>Naziv tvrtke</th>
					<th>OIB</th>
					<th>Adresa sjedišta</th>
					<th>Telefon</th>
					<th>Mobitel</th>
					<th>Email</th>
					<th>Unos</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($cooperants as $cooperant)
					<tr>
						<td>{{ $cooperant->getFullName() }}</td>
						<td>{{ $cooperant->company }}</td>
						<td>{{ $cooperant->OIB }}</td>
						<td>{{ $cooperant->address->getCityCountry() }}</td>
						<td>{{ $cooperant->phone }}</td>
						<td>{{ $cooperant->mobile_phone }}</td>
						<td>{{ $cooperant->email }}</td>
						<td>{{ $cooperant->created_at->format('d.m.Y.') }}</td>
						<td><a href="{{ route('cooperants.edit', $cooperant->id) }}">
								<i class="fa fa-gear"></i>&nbsp;
							</a>
							<a class="delete-resource" data-resId="{{ $cooperant->id }}" href="{{ route('cooperants.destroy', $cooperant->id) }}"><i class="fa fa-times"></i></a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
@endsection
