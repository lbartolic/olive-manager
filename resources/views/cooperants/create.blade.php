@extends("layouts.master")

@section("title", "Kooperanti")

@section("main-content")
   <div class="pc no--br">
      <div class="panel-body">
         <h1>Novi kooperant</h1>
         {!! Form::open(['method' => 'POST', 'action' => 'CooperantController@store']) !!}
         @include('cooperants.includes.form', ['submitBtnText' => 'Spremi'])
         {!! Form::close() !!}
      </div>
   </div>
@endsection