@extends("layouts.master")

@section("title", "Kooperanti")

@section("main-content")
   <div class="pc no--br">
      <div class="panel-body">
         <h1>{{ $cooperant->getFullName() }}</h1>
         {!! Form::open(['method' => 'PATCH', 'action' => ['CooperantController@update', $cooperant->id]]) !!}
         	@include('cooperants.includes.form', ['submitBtnText' => 'Spremi izmjene'])
         {!! Form::close() !!}
      </div>
   </div>


@endsection