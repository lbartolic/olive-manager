<div class="row">
   <div class="col-sm-5">
      <div class="form-group">
         <label for="first_name">Ime</label>
         <input type="text" class="fc" name="first_name" id="first_name" placeholder="Ime" value="{{ $cooperant->first_name or old('first_name') }}">
      </div>
      <div class="form-group">
         <label for="last_name">Prezime</label>
         <input type="text" class="fc" name="last_name" id="last_name" placeholder="Prezime" value="{{ $cooperant->last_name or old('last_name') }}">
      </div>
      <div class="form-group">
         <label for="company">Tvrtka</label>
         <input type="text" class="fc" name="company" id="company" placeholder="Tvrtka" value="{{ $cooperant->company or old('company') }}">
      </div>
      <div class="form-group">
         <label for="OIB">OIB</label>
         <input type="text" class="fc" name="OIB" id="OIB" placeholder="OIB" value="{{ $cooperant->OIB or old('OIB') }}">
      </div>
      <div class="form-group">
         <label for="street">Ulica</label>
         <input type="text" class="fc" name="street" id="street" placeholder="Ulica" value="{{ $cooperant->address->street or old('street') }}">
      </div>
      <div class="form-group">
         <label for="city">Grad</label>
         <input type="text" class="fc" name="city" id="city" placeholder="Grad" value="{{ $cooperant->address->city or old('city') }}">
      </div>
   </div>
   <div class="col-sm-5">
      <div class="form-group">
         <label for="country">Država</label>
         <input type="text" class="fc" name="country" id="country" placeholder="Država" value="{{ $cooperant->address->country or old('country') }}">
      </div>
      <div class="form-group">
         <label for="postal_code">Poštanski broj</label>
         <input type="text" class="fc" name="postal_code" id="postal_code" placeholder="Poštanski broj" value="{{ $cooperant->address->postal_code or old('postal_code') }}">
      </div>
      <div class="form-group">
         <label for="phone">Telefon</label>
         <input type="text" class="fc" name="phone" id="phone" placeholder="Telefon" value="{{ $cooperant->phone or old('phone') }}">
      </div>
      <div class="form-group">
         <label for="mobile_phone">Mobitel</label>
         <input type="text" class="fc" name="mobile_phone" id="mobile_phone" placeholder="Mobitel" value="{{ $cooperant->mobile_phone or old('mobile_phone') }}">
      </div>
      <div class="form-group">
         <label for="email">Email</label>
         <input type="text" class="fc" name="email" id="email" placeholder="Email" value="{{ $cooperant->email or old('email') }}">
      </div>
      <div class="form-group">
         <label for="comment">Komentar</label>
         <input type="text" class="fc" name="comment" id="comment" placeholder="Komentar" value="{{ $cooperant->comment or old('comment') }}">
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <button class="btn-cst" type="submit">{{ $submitBtnText }}</button>
   </div>
</div>