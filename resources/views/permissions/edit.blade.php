@extends("layouts.master")

@section("title", "Dodaj dopuštenje")

@section("main-content")
    <div class="pc no--br">
        <div class="panel-body">
            <form action="{{ route('permissions.update', $permission->id) }}" method="POST">
                {!! method_field("PATCH") !!}
                <div class="form-group">
                    <label for="title">Naziv dopuštenja</label>
                    <input value="{{ $permission->title }}" type="text" class="fc" name="title" id="title" placeholder="Naziv dopuštenja">
                </div>
                {!! csrf_field() !!}
                <button type="submit" class="btn-cst">Izmjeni dopuštenje</button>
            </form>
        </div>
    </div>
@endsection
