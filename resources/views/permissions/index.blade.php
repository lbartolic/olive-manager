@extends("layouts.master")

@section("title", "Dodaj dopuštenje")

@section("main-content")
    <table class="table-custom">
        <thead>
        <tr>
            <th>Ime dopuštenja</th>
            <th>Mogućnosti</th>
        </tr>
        </thead>
        <tbody>
        @foreach($permissions as $permission)
            <tr>
                <td>{{ $permission->title }}</td>
                <td><a href="{{ route('permissions.edit', $permission->id) }}">
                        <i class="fa fa-gear"></i>&nbsp;
                    </a>
                    <a class="delete-resource" data-resId="{{ $permission->id }}" href="{{ route('permissions.destroy', $permission->id) }}"><i class="fa fa-times"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
