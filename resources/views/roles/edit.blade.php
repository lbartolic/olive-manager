@extends("layouts.master")

@section("title", "Izmjeni ulogu")

@section("main-content")
    <div class="pc no--br">
        <div class="panel-body">
            <form action="{{ route('roles.update', $role->id) }}" method="POST">
                {!! method_field("PATCH") !!}
                <div class="form-group">
                    <label for="title">Naziv uloge</label>
                    <input value="{{ $role->title }}" type="text" class="fc" name="title" id="title" placeholder="Naziv uloge">
                </div>
                <h4>Dopuštenja</h4>
                @foreach($permissions as $permission)
                    <div class="checkbox">
                        <label>
                            <input
                                    @if(in_array($permission->id, $role->permissions->pluck('id')->toArray()))
                                    checked
                                    @endif
                                    name="permissions[]"
                                    value="{{ $permission->id }}"
                                    type="checkbox" id="{{ $permission->slug }}"> {{ $permission->title }}
                        </label>
                    </div>
                @endforeach
                <button type="submit" class="btn-cst">Izmjeni ulogu</button>
                {!! csrf_field() !!}
            </form>
        </div>
    </div>
@endsection
