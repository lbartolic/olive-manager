@extends("layouts.master")

@section("title", "Dodaj ulogu")

@section("main-content")
    <div class="pc no--br">
        <div class="panel-body">
            <form action="{{ route('roles.store') }}" method="POST">
                <div class="form-group">
                    <label for="title">Naziv uloge</label>
                    <input type="text" class="fc" name="title" id="title" placeholder="Naziv uloge">
                </div>
                <h4>Dopuštenja</h4>
                @foreach($permissions as $permission)
                    <div class="checkbox">
                        <label>
                            <input name="permissions[]" value="{{ $permission->id }}" type="checkbox" id="{{ $permission->slug }}"> {{ $permission->title }}
                        </label>
                    </div>
                @endforeach
                <button type="submit" class="btn-cst">Dodaj ulogu</button>
                {!! csrf_field() !!}
            </form>
        </div>
    </div>
@endsection
