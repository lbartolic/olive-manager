@extends("layouts.master")

@section("title", "Pregledaj uloge")

@section("main-content")
    <table class="table-custom">
        <thead>
            <tr>
                <th>Ime uloge</th>
                <th>Mogućnosti</th>
            </tr>
        </thead>
        <tbody>
        @foreach($roles as $role)
            <tr>
                <td>{{ $role->title }}</td>
                <td><a href="{{ route('roles.edit', $role->id) }}">
                        <i class="fa fa-gear"></i>&nbsp;
                    </a>
                    <a class="delete-resource" data-resId="{{ $role->id }}" href="{{ route('roles.destroy', $role->id) }}"><i class="fa fa-times"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
