@extends("layouts.master")

@section("main-content")
  <div class="row">
    <div class="col-md-3">
      <div class="pc no--br">
        <div class="panel-body" text-center"">
          <form>
            <input type="email" class="fc no--br" placeholder="Email">
            <input type="password" class="fc no--br"  placeholder="Password">
            <input type="password" class="fc no--br"  placeholder="Confirm password">
            <button type="submit" class="btn-cst no--br">Login</button>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-9">
      <div class="bs-example">
        <table class="table-custom">
          <thead>
            <tr>
              <th class="accent">#</th>
              <th class="accent">First Name</th>
              <th class="accent">Last Name</th>
              <th class="accent">Edit</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
              <td><a href="#"><i class="fa fa-gear"></i>&nbsp;</a> <a href="#"><i class="fa fa-times"></i></a></td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>Thornton</td>
              <td><a href="#"><i class="fa fa-gear"></i>&nbsp;</a> <a href="#"><i class="fa fa-times"></i></a></td><tr>
              <th scope="row">3</th>
              <td>Larry</td>
              <td>the Bird</td>
              <td><a href="#"><i class="fa fa-gear"></i>&nbsp;</a> <a href="#"><i class="fa fa-times"></i></a></td></tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
