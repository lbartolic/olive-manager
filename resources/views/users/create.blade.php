@extends("layouts.master")

@section("title", "Korisnici")

@section("main-content")
    <div class="pc no--br">
        <div class="panel-body">
            <h1>Novi korisnik</h1>
            <form action="{{ route('users.store') }}" method="POST">
                <div class="form-group">
                    <label for="name">Ime</label>
                    <input type="text" class="fc" name="name" id="name" placeholder="Ime korisnika">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="fc" name="email" id="email" placeholder="Email korisnika">
                </div>
                <div class="form-group">
                    <label for="password">Lozinka</label>
                    <input type="password" class="fc" name="password" id="password" placeholder="Lozinka korisnika">
                </div>

                <div class="form-group">
                    <label for="role_id">Uloga</label>
                    {!! Form::select('role_id', $roles, null, ['class' => 'fc']) !!}
                </div>

                <button type="submit" class="btn-cst">Dodaj korisnika</button>
                {!! csrf_field() !!}
            </form>
        </div>
    </div>
@endsection