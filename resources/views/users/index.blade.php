@extends("layouts.master")

@section("title", "Pregledaj uloge")

@section("main-content")
    <table class="table-custom">
        <thead>
            <tr>
                <th>Ime Korisnika</th>
                <th>Email</th>
                <th>Uloga</th>
                <th>Mogućnosti</th>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->name}}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->role->title }}</td>
                <td><a href="{{ route('users.edit', $user->id) }}">
                        <i class="fa fa-gear"></i>&nbsp;
                    </a>
                    <a class="delete-resource" data-resId="{{ $user->id }}" href="{{ route('users.destroy', $user->id) }}"><i class="fa fa-times"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
