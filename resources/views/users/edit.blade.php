@extends("layouts.master")

@section("title", "Korisnici")

@section("main-content")
    <div class="pc no--br">
        <div class="panel-body">
            <h1>Novi korisnik</h1>
            <form action="{{ route('users.update', $user->id) }}" method="POST">
                {!! method_field('PATCH') !!}
                <div class="form-group">
                    <label for="name">Ime</label>
                    <input value="{{ $user->name }}" type="text" class="fc" name="name" id="name" placeholder="Ime korisnika">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input value="{{ $user->email }}" type="email" class="fc" name="email" id="email" placeholder="Email korisnika">
                </div>
                <div class="form-group">
                    <label for="password">Lozinka</label>
                    <input type="text" class="fc" name="password" id="password" placeholder="Lozinka korisnika">
                </div>

                <div class="form-group">
                    <label for="role_id">Uloga</label>
                    {!! Form::select('role_id', $roles, $user->role->id, ['class' => 'fc']) !!}
                </div>

                <button type="submit" class="btn-cst">Dodaj korisnika</button>
                {!! csrf_field() !!}
            </form>
        </div>
    </div>
@endsection