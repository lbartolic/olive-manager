<div class="col-md-3">
  <div class="panel element-style">
    
    <a href="#menu3" class="lgi border-none" data-toggle="collapse" data-parent="#MainMenu">
      <i class="fa fa-calendar-plus-o"></i>&nbsp; Predbilježbe <i class="fa fa-caret-down pull-right"></i>
    </a>
    <div class="collapse" id="menu3">
      <a href="{{ route('bookings.create') }}" class="lgi">Dodaj predbilježbu</a>
      <a href="{{ route('bookings.index') }}" class="lgi">Pregledaj predbilježbe</a>
    </div>

    @if(Auth::user()->hasPermission('upravljanje-obradom-maslina'))
      <a href="#menu5" class="lgi border-none" data-toggle="collapse" data-parent="#MainMenu">
        <i class="fa fa-lemon-o "></i>&nbsp; Obrada masline <i class="fa fa-caret-down pull-right"></i>
      </a>
      <div class="collapse" id="menu5">
        <a href="{{ route('processing.index') }}" class="lgi">Pregledaj obrade</a>
        <a href="{{ route('processing.create') }}" class="lgi">Dodaj obradu</a>
      </div>
    @endif

    @if(Auth::user()->hasRole('administrator'))
      <a href="#menu2" class="lgi border-none" data-toggle="collapse" data-parent="#MainMenu">
        <i class="fa fa-key"></i>&nbsp; Upravljanje pristupom <i class="fa fa-caret-down pull-right"></i>
      </a>
      <div class="collapse" id="menu2">
        <a href="{{ route('users.index') }}" class="lgi">Korisnici</a>
        <a href="{{ route('roles.index') }}" class="lgi">Uloge</a>
        <a href="{{ route('permissions.index') }}" class="lgi">Dopuštenja</a>
      </div>
      <a href="{{ route('users.index') }}" class="lgi border-none"  data-parent="#MainMenu">
        <i class="fa fa-key"></i>&nbsp; Korisnici
      </a>
    @endif


  </div>
</div>
