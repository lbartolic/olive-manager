<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/favicon.ico">

    <title>@yield("title")</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <!-- Font awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    @yield("head-styles")

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield("head-scripts")
  </head>

  <body class="body">
    <!-- MAIN CONTAINER -->
    <div class="main-wrapper">
      <nav>
        <div class="navigation">
          <ul>
            <li><img src="img/logo.png" alt="olivio" /></li>
            <li><a href="#">Prijava</a></li>
          </ul>
        </div>
      </nav>
      <div class="welcome-wrapper">
        <h2>Prati kooperante i uvjeri se da su tvoje masline na dobrom putu.</h2>
        <form class="forma">
          <input type="email" class="simple no--br" placeholder="Email" name="email">
          <input type="password" class="simple no--br"  placeholder="Lozinka" name="password">
          <input type="password" class="simple no--br"  placeholder="Potvrdi lozinku" name="password">
          <button type="submit" class="btn-ghost no--br">Prijavi se</button>
        </form>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    @yield("end-body-scripts")
  </body>
</html>
