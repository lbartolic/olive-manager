@extends("layouts.master")

@section("title", "Predbilježbe")

@section("main-content")
   <div class="pc no--br">
        <div class="panel-body">
      <h1>Nova predbilježba</h1>
      {!! Form::open(['method' => 'POST', 'action' => 'BookingController@store']) !!}
         @include('bookings.includes.form', ['submitBtnText' => 'Spremi'])
      {!! Form::close() !!}
      </div>
      </div>
   </div>
@endsection