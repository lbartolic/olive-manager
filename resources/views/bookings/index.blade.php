@extends("layouts.master")

@section("title", "Predbilježbe")

@section("main-content")
		<table class="table-custom">
			<thead>
				<tr>
					<th>Kooperant</th>
					<th>Adresa kooperanta</th>
					<th>Adresa maslina</th>
					<th>Prerada</th>
					<th>Količina maslina</th>
					<th>Transport</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($bookings as $booking)
					<tr>
						<td>{{ $booking->cooperant->getFullName() }}</td>
						<td>{{ $booking->cooperant->address->getCityCountry() }}</td>
						<td>{{ $booking->address->getCityCountry() }}</td>
						<td>{{ $booking->processing_start->format('d.m.Y. H:m:s') }}</td>
						<td>{{ $booking->olive_amount }}</td>
						<td>@if($booking->transport == 1) {{ 'DA' }} @else {{ 'NE' }} @endif</td>
						<td><a href="{{ route('bookings.edit', $booking->id) }}">
								<i class="fa fa-gear"></i>&nbsp;
							</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
@endsection
