@extends("layouts.master")

@section("title", "Predbilježbe")

@section("main-content")
	<h1 class="h1--lead">Lokacije maslina <small>za trenutni dan, {{ $today }}</small></h1>
	<h3 class="h1--lead">Broj: <b><span id="brMaslina"></span></b> <small><a href="{{ route('bookings.index') }}">vidi sve</a></small></h3>
	<div id="map" style="height: 400px; width: 100%;"></div>
@endsection

@section('end-body-scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB30atU6bjQnqri5neECcI5aewC7qX6wA8&signed_in=true&libraries=geometry"></script>
<script>
	var APP_URL = {!! json_encode(url('/')) !!};
	$.get(APP_URL + '/bookings/getMapJSON', function(r) {})
	.done(function(r) {
		$('#brMaslina').html(r.length);

		var map = new google.maps.Map(document.getElementById('map'), {
	    	zoom: 2,
	      	//center:
	      	disableDefaultUI: true,
	      	zoomControl:true
   		});
   		var markers = [];
	   	for (var i=0 ; i<r.length ; i++) {
			var geocoder = new google.maps.Geocoder();
			var addr = r[i].address.city + ', ' + r[i].address.country;
			geocoder.geocode({ 'address': addr }, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					var lat = results[0].geometry.location.lat();
					var lng = results[0].geometry.location.lng();
				   	var placesCoords = [{ lat: lat, lng: lng }];
				      	markers.push(new google.maps.Marker({
				        	position: placesCoords[0],
				         	map: map,
				         	animation: google.maps.Animation.DROP
				      	}));

				   	var bounds = new google.maps.LatLngBounds();
				   	for(i=0; i<markers.length; i++) {
				      	bounds.extend(markers[i].getPosition());
				   	}

				   	map.fitBounds(bounds);
				   	var listener = google.maps.event.addListener(map, "idle", function() { 
				      	if (map.getZoom() > 9) map.setZoom(9); 
				      	google.maps.event.removeListener(listener); 
				   	});
				}
			});
		}
	}, "json");





</script>
@endsection