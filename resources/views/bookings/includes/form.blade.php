@section('head-styles')
   <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@endsection

<div class="row">
   <div class="col-sm-5">
      <div class="form-group">
         <label for="cooperant_id">Kooperant</label>
         <select class="form-control" name="cooperant_id" id="cooperant_id">
            @foreach($cooperants as $cooperant)
               <option value="{{ $cooperant->id }}">{{ $cooperant->getFullName() }}</option>
            @endforeach
         </select>
      </div>
      <div class="checkbox">
         <label>
            <input type="checkbox" id="address-hidden-chk" name="address_hidden" id="address_hidden" checked> Adresa ista kao sjedište<br>
         </label>
      </div>
      <div id="address-hidden" class="_address-hidden" style="display: none;">
         <div class="form-group">
            <label for="street">Ulica</label>
            <input type="text" class="form-control" name="street" id="street" placeholder="Ulica" value="{{ $booking->address->street or old('street') }}">
         </div>
         <div class="form-group">
            <label for="city">Grad</label>
            <input type="text" class="form-control" name="city" id="city" placeholder="Grad" value="{{ $booking->address->city or old('city') }}">
         </div>
         <div class="form-group">
            <label for="country">Država</label>
            <input type="text" class="form-control" name="country" id="country" placeholder="Država" value="{{ $booking->address->country or old('country') }}">
         </div>
         <div class="form-group">
            <label for="postal_code">Poštanski broj</label>
            <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="Poštanski broj" value="{{ $booking->address->postal_code or old('postal_code') }}">
         </div>
      </div>
   </div>
   <div class="col-sm-5">
      <div class="form-group">
         <label for="olive_amount">Količina maslina [kg]</label>
         <input type="number" class="form-control" name="olive_amount" id="olive_amount" placeholder="Količina maslina u kilogramima" value="{{ $booking->olive_amount or old('company') }}">
      </div>
      <div class="form-group">
         <label for="transport">Transport</label>
         <select class="form-control" name="transport" id="transport">
            <option value="1" @if (isset($booking) && ($booking->transport == 1)) {{ 'selected' }} @endif>Da</option>
            <option value="0" @if (isset($booking) && $booking->transport == 0) {{ 'selected' }} @endif>Ne</option>
         </select>
      </div>
      <div class="form-group">
         <label for="processing_start">Datum i vrijeme prerade</label>
         <div class='input-group date' id='processing_start_group'>
            <input type='text' class="form-control" name="processing_start" id="processing_start" value="{{ $booking->processing_start or old('processing_start') }}"/>
            <span class="input-group-addon">
               <span class="glyphicon glyphicon-calendar"></span>
            </span>
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-sm-12">
      <button type="submit" class="btn-cst">{{ $submitBtnText }}</button>
   </div>
</div>

@section('end-body-scripts')
   <script src="{{ asset('js/moment.js') }}"></script>
   <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>

   <script>
      $('#address-hidden-chk').click(function() {
         if(document.getElementById('address-hidden-chk').checked) {
            $("#address-hidden").fadeOut();
         } else {
            $("#address-hidden").fadeIn();
         }
      });

      $('#processing_start_group').datetimepicker({
         format: 'Y-M-D HH:mm:ss'
      });
   </script>
@endsection