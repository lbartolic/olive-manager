@extends("layouts.master")

@section("title", "Predbilježbe")

@section("main-content")
   <div class="pc no--br">
        <div class="panel-body">
	      	{!! Form::open(['method' => 'PATCH', 'action' => ['BookingController@update', $booking->id]]) !!}
	        	@include('bookings.includes.form', ['submitBtnText' => 'Spremi izmjene'])
	      	{!! Form::close() !!}
	      </div>
      </div>
   </div>
@endsection