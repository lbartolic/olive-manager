@extends("layouts.master")

@section("title", "Dodaj obradu")

@section('head-styles')
    <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@endsection

@section("main-content")
    <div class="pc no--br">
        <div class="panel-body">
            <form action="{{ route('processing.store') }}" method="POST">
                <div class="form-group">
                    <label for="booking_id">Predbilježba</label>
                    {!! Form::select('booking_id', $bookings, null, ['class' => 'fc']) !!}
                </div>
                <div class="form-group">
                    <label for="processing_start">Početak prerade</label>
                    <div class='input-group date' id='processing_start'>
                        <input type='text' class="form-control" name="processing_start"/>
            <span class="input-group-addon">
               <span class="glyphicon glyphicon-calendar"></span>
            </span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="processing_start">Kraj prerade</label>
                    <div class='input-group date' id='processing_end'>
                        <input type='text' class="form-control" name="processing_end"/>
                    <span class="input-group-addon">
                       <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="olive_amount">Količina masline (kg)</label>
                    <input type="text" class="fc" name="olive_amount" id="olive_amount" placeholder="Količina masline">
                </div>

                <div class="form-group">
                    <label for="olive_quality_id">Kvaliteta masline</label>
                    {!! Form::select('olive_quality_id', $oil_qualities, null, ['class' => 'fc']) !!}
                </div>

                <div class="form-group">
                    <label for="oil_amount">Količina maslinovog ulja (L)</label>
                    <input type="text" class="fc" name="oil_amount" id="oil_amount" placeholder="Količina maslinovog ulja">
                </div>

                <div class="form-group">
                    <label for="oil_quality_id">Kvaliteta masline</label>
                    {!! Form::select('oil_quality_id', $olive_qualities, null, ['class' => 'fc']) !!}
                </div>


                {!! csrf_field() !!}
                <button type="submit" class="btn-cst">Dodaj obradu</button>
            </form>
        </div>
    </div>
@endsection

@section('end-body-scripts')
    <script src="{{ asset('js/moment.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>

    <script>

        $('#processing_start').datetimepicker({
            format: 'Y-M-D HH:mm:ss'
        });

        $('#processing_end').datetimepicker({
            format: 'Y-M-D HH:mm:ss'
        });
    </script>
@endsection