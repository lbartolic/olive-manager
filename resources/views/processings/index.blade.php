@extends("layouts.master")

@section("title", "Obrade")

@section("main-content")
		<table class="table-custom">
			<thead>
				<tr>
					<th>ID</th>
					<th>ID predbiljedžba</th>
					<th>Adresa kooperanta</th>
					<th>Količina maslina</th>
					<th>Količina ulja</th>
					<th>Kvaliteta maslina</th>
					<th>Kvaliteta ulja</th>
					<th>Mogućnosti</th>
				</tr>
			</thead>
			<tbody>
				@foreach($processings as $processing)
					<tr>
						<td>{{ $processing->id }}</td>
						<td>{{ $processing->booking_id }}</td>
						<td>{{ $processing->booking->cooperant->address->getCityCountry() }}</td>
						<td>{{ $processing->olive_amount }}</td>
						<td>{{ $processing->oil_amount }}</td>
						<td>{{ $processing->oilQuality->quality }}</td>
						<td>{{ $processing->oliveQuality->quality }}</td>
						<td><a href="{{ route('processing.edit', $processing->id) }}">
								<i class="fa fa-gear"></i>&nbsp;
							</a>
							<a class="delete-resource" data-resId="{{ $processing->id }}" href="{{ route('processing.destroy', $processing->id) }}"><i class="fa fa-times"></i></a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
@endsection
