<?php

namespace App\Http\Requests\Bookings;

use App\Http\Requests\Request;

class CreateBookingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'cooperant_id' => 'required|integer|exists:cooperants,id',
            'processing_start' => 'required',
            'olive_amount' => 'required|integer',
            'transport' => 'required|in:0,1',
            'street' => 'required_unless:address_hidden,on',
            'city' => 'required_unless:address_hidden,on',
            'country' => 'required_unless:address_hidden,on',
            'postal_code' => 'required_unless:address_hidden,on'
        ];
        return $rules;
    }
}
