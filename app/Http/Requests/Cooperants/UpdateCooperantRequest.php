<?php

namespace App\Http\Requests\Cooperants;

use App\Http\Requests\Request;

class UpdateCooperantRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'company' => 'required',
            'OIB' => 'required',
            'phone' => 'required',
            'mobile_phone' => 'required',
            'email' => 'required',
            'comment' => 'required'
        ];
        return $rules;
    }
}
