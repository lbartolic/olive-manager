<?php

namespace App\Http\Requests\Roles;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class StoreRoleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole('administrator');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|alpha|unique:roles,title',
            'permissions' => 'required|array'
        ];
    }
}
