<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\CooperantRepository;

use App\Cooperant;
use App\Address;

use App\Http\Requests;
use App\Http\Requests\Cooperants\UpdateCooperantRequest;
use App\Http\Requests\Cooperants\CreateCooperantRequest;

class CooperantController extends Controller
{

    private $cooperantsRepo;

    public function __construct(CooperantRepository $cooperants) {
        $this->cooperantsRepo = $cooperants;
    }

    public function test() {
        $cooperants = $this->cooperantsRepo->getCooperants();
        return view('test');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cooperants = $this->cooperantsRepo->getCooperants();
        return view('cooperants.index')->with('cooperants', $cooperants);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cooperants.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCooperantRequest $request)
    {
        $data = $request->all();

        $address = Address::create($data);
        $data['address_id'] = $address->id;

        $cooperant = Cooperant::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cooperant = $this->cooperantsRepo->getCooperantById($id);
        return view('cooperants.edit')->with('cooperant', $cooperant);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCooperantRequest $request, $id)
    {
        $cooperant = $this->cooperantsRepo->getCooperantById($id);
        
        $data = $request->all();

        $cooperant->address->fill($data)->save();
        $cooperant->update($data);

        return redirect()->route('cooperants.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cooperant::destroy($id);
    }
}
