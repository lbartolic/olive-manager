<?php

namespace App\Http\Controllers;

use App\Booking;
use App\OilQuality;
use App\OliveQuality;
use App\Processing;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProcessingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('processings.index', [
            'processings' => Processing::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bookings = Booking::latest()->get();

        foreach($bookings as $booking){
            $bookingKey = ($booking->processing_start->toDateTimeString() . ', '. $booking->cooperant->getFullname());
            $bookingsData[$booking->id] = $bookingKey;
        }

        return view('processings.create', [
            'bookings' => $bookingsData,
            'olive_qualities' => OilQuality::all()->pluck('quality', 'id'),
            'oil_qualities' => OliveQuality::all()->pluck('quality', 'id')

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $booking = Processing::create($request->all());

        return redirect()->route('processing.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bookings = Booking::latest()->get();

        foreach($bookings as $booking){
            $bookingKey = ($booking->processing_start->toDateTimeString() . ', '. $booking->cooperant->getFullname());
            $bookingsData[$booking->id] = $bookingKey;
        }

        return view('processings.edit', [
            'bookings' => $bookingsData,
            'olive_qualities' => OilQuality::all()->pluck('quality', 'id'),
            'oil_qualities' => OliveQuality::all()->pluck('quality', 'id'),
            'processing' => Processing::findOrFail($id)

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Processing::find($id)->fill($request->all())->save();

        return redirect()->route('processing.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Processing::destroy($id);
    }
}
