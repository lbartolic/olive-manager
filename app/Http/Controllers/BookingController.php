<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cooperant;
use App\Address;
use App\Booking;

use App\Http\Requests;
use App\Http\Requests\Bookings\CreateBookingRequest;
use App\Http\Requests\Bookings\EditBookingRequest;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = Booking::latest()->get();
        return view('bookings.index')->with('bookings', $bookings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cooperants = Cooperant::all();
        return view('bookings.create')->with('cooperants', $cooperants);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBookingRequest $request)
    {
        $data = $request->all();

        $cooperant = Cooperant::find($request->input('cooperant_id'));

        ($request->has('address_hidden')) ? $address = $cooperant->address : $address = Address::create($data);
        $data['address_id'] = $address->id;

        $booking = Booking::create($data);

        return redirect()->route('bookings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cooperants = Cooperant::all();
        $booking = Booking::find($id);
        return view('bookings.edit')->with('cooperants', $cooperants)
            ->with('booking', $booking);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditBookingRequest $request, $id)
    {
        $data = $request->all();

        $booking = Booking::find($id);

        $cooperant = Cooperant::find($request->input('cooperant_id'));

       if (!($request->has('address_hidden'))) $booking->address->fill($data)->save();

        $booking->update($data);

        return redirect()->route('bookings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getMap() {
        return view('bookings.map')->with('today', \Carbon\Carbon::today()->format('d.m.Y.'));
    }

    public function getMapJSON() {
        $bookings = Booking::where('transport', '1')
            ->with('address')
            ->where('processing_start', '>', \Carbon\Carbon::yesterday())
            ->where('processing_start', '<', \Carbon\Carbon::tomorrow())
            ->get();

        return response()->json($bookings);
    }
}
