<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('/test', 'HomeController@test');

Route::resource('roles', 'RoleController');
Route::resource('permissions', 'PermissionController');

Route::resource('cooperants', 'CooperantController');
Route::get('bookings/map', 'BookingController@getMap');
Route::get('bookings/getMapJSON', 'BookingController@getMapJSON');
Route::resource('bookings', 'BookingController');

Route::resource('processing', 'ProcessingController');

Route::resource('config', 'ConfigController');

Route::resource('users', 'UserController');

Route::get('/luka', 'CooperantController@test');

Route::get('/landing', function(){
   return view('layouts.landingmaster');
});

