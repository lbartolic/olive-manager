<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Processing extends Model
{
    protected $fillable = ['oil_amount', 'olive_amount', 'olive_quality_id', 'booking_id',
        'oil_quality_id', 'processing_start', 'processing_end'];

    public function oliveQuality() {
    	return $this->belongsTo('App\OliveQuality');
    }

    public function oilQuality() {
    	return $this->belongsTo('App\OilQuality');
    }

    public function booking(){
        return $this->belongsTo('App\Booking');
    }
}
