<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OilQuality extends Model
{
    protected $table = "olive_quality";
}
