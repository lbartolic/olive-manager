<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role() {
        return $this->belongsTo('App\Role');
    }

    public function hasPermission($key)
    {
        if($this->role->hasPermission($key)){
            return true;
        }

        return false;
    }

    public function hasRole($key)
    {
        if($this->role->slug == $key){
            return true;
        }

        return false;
    }
}
