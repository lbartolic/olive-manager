<?php namespace App\Repositories;

use Illuminate\Http\Request;

use App\Cooperant;

use Auth;

class CooperantRepository {
	public function getCooperants() {
		$cooperants = Cooperant::latest()->get();
		return $cooperants;
	}

	public function getCooperantById($id) {
		return Cooperant::findOrFail($id);
	}
}