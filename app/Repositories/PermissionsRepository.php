<?php

namespace App\Repositories;

use App\Permission;

class PermissionsRepository
{
    public function all()
    {
         return Permission::all();
    }

    public function findById($id)
    {
        return Permission::findOrFail($id);
    }

    public function store($data)
    {
        return Permission::create($data);
    }

    public function update($id, $data)
    {
        $permission = $this->findById($id);
        $permission->fill($data)->save();
    }

}