<?php

namespace App\Repositories;

use App\Role;
use Illuminate\Support\Facades\DB;

class RolesRepository
{
    public function all()
    {
        return Role::all();
    }

    public function findById($id)
    {
        return Role::findOrFail($id);
    }

    public function store($data)
    {
        $role = Role::create(['title' => $data['title']]);

        foreach ($data['permissions'] as $permission) {
            DB::table('role_permission')->insert(['role_id' => $role->id, 'permission_id' => (int)$permission]);
        }

        return $role;
    }

    public function update($id, $data)
    {
        $role = $this->findById($id)->fill($data);
        $role->save();

        if(isset($data['permissions']))
        {
            DB::table('role_permission')->where('role_id', $role->id)->delete();
            foreach ($data['permissions'] as $permission) {
                DB::table('role_permission')->insert(['role_id' => $role->id, 'permission_id' => (int)$permission]);
            }
        }

        return $role;
    }
}