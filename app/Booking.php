<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
	protected $fillable = [
        'cooperant_id',
        'address_id',
        'processing_start',
        'olive_amount',
        'transport'
    ];

    protected $dates = [
    	'processing_start'
    ];

    public function cooperant() {
    	return $this->belongsTo('App\Cooperant');
    }

    public function address() {
    	return $this->belongsTo('App\Address');
    }
}
