<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OliveQuality extends Model
{
    protected $table = "oil_quality";
}
