<?php

namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $fillable = ['title', 'slug'];
    
    public function roles() {
    	return $this->belongsToMany('App\Role', 'role_permission')->withTimestamps();
    }
}
