<?php

namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Role extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $fillable = ['title', 'slug'];
    
    public function users() {
    	return $this->belongsToMany('App\User');
    }

    public function permissions() {
    	return $this->belongsToMany('App\Permission', 'role_permission')->withTimestamps();
    }

    public function hasPermission($key){
        if(in_array($key, $this->permissions->pluck("slug")->toArray())){
            return true;
        }

        return false;
    }
}
