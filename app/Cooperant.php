<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cooperant extends Model
{

	protected $fillable = [
        'first_name',
        'last_name',
        'company',
        'OIB',
        'phone',
        'mobile_phone',
        'email',
        'comment',
        'address_id'
    ];
    
    public function bookings()
    {
    	return $this->hasMany('App\Booking');
    }

    public function address()
    {
    	return $this->belongsTo('App\Address');
    }

    public function getFullName() {
    	return $this->first_name . ' ' . $this->last_name;
    }
}
